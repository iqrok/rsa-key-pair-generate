const crypto = require("crypto");
const path = require("path");
const fs = require("fs");

class Enc {
	constructor( config ) {
		this.config = config;
	}
	encRsa(toEncrypt) {
		let absolutePath = path.resolve(this.config['pubkey']);
		let publicKey = fs.readFileSync(absolutePath, "utf8");
		let buffer = Buffer.from(toEncrypt);
		let encrypted = crypto.publicEncrypt({key:publicKey,passphrase:''}, buffer);
		return encrypted.toString("base64");
	}

	decRsa(toDecrypt){
		let absolutePath = path.resolve(this.config['privkey']);
		let privateKey = fs.readFileSync(absolutePath, "utf8");
		let buffer = Buffer.from(toDecrypt, "base64");
		let decrypted = crypto.privateDecrypt({key:privateKey,passphrase:''}, buffer);
		return decrypted.toString("utf8");
	}

	encrypt(toEncrypt){
		let cipher = crypto.createCipheriv(this.config['algorithm'],this.config['password'],this.config['iv'])
		let crypted = cipher.update(toEncrypt,'utf8','hex')
		crypted += cipher.final('hex');
		return crypted;
	}

	decrypt(toDecrypt){
		let decipher = crypto.createDecipheriv(this.config['algorithm'],this.config['password'],this.config['iv'])
		let dec = decipher.update(toDecrypt,'hex','utf8')
		dec += decipher.final('utf8');
		return dec;
	}

	encB64(toEncode){
		let buffer = Buffer.from(toEncode, "utf8");
		return buffer.toString("base64");
	}
	decB64(toDecode){
		let buffer = Buffer.from(toDecode, "base64");
		return buffer.toString("utf8");
	}
}
module.exports = Enc;
