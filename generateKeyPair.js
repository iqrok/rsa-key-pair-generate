const { generateKeyPair } = require('crypto');
const fs = require('fs');

function writeToFile(location,toWrite){
	const data = new Uint8Array(Buffer.from(toWrite));
	fs.writeFile(location, data, (err) => {
		if (err) throw err;
		console.log('The file has been saved!');
	});
}


const config = {
	algorithm : process.argv[2] || 'rsa',
	modulusLength : +process.argv[3] || 4096,
	passphrase : process.argv[4] || '',
};

generateKeyPair(config.algorithm, {
  modulusLength: config.modulusLength,
  publicKeyEncoding: {
    type: 'spki',
    format: 'pem'
  },
  privateKeyEncoding: {
    type: 'pkcs8',
    format: 'pem',
    cipher: 'aes-256-cbc',
    passphrase: config.passphrase
  }
}, (err, publicKey, privateKey) => {
	if(err){
		throw err;
	}

	console.log(publicKey);
	console.log('');
	console.log(privateKey);
	console.log('');

	writeToFile('public.pem',publicKey);
	writeToFile('private.pem',privateKey);
});
