const __enc = require('./enc.class.js');
const configs = {
		pubkey : './public.pem',
		privkey : './private.pem',
	};
const enc = new __enc(configs);

const messages = "I would recommend not using synchronous fs methods where possible, and you could use Promises to make this better, but for simple use cases this is the approach that I have seen work and would take";

console.time('EncryptTime');
const encrypted = enc.encRsa(messages);
console.timeEnd('EncryptTime');
console.log(encrypted);
console.log('-------------------------------------');

console.time('DecryptTime');
const decrypted = enc.decRsa(encrypted);
console.timeEnd('DecryptTime');
console.log(decrypted);
console.log('-------------------------------------');
console.log( (decrypted==messages)?("SUCCESS"):("failed") );
